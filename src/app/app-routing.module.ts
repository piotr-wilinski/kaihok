import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        loadChildren: (): any => import('./homepage/homepage.module').then((m: any) => m.HomepageModule),
    },
    {
        path: 'services/online-marketing',
        loadChildren: (): any =>
            import('./online-marketing/online-marketing.module').then((m: any) => m.OnlineMarketingModule),
    },
    {
        path: 'services/design',
        loadChildren: (): any => import('./design/design.module').then((m: any) => m.DesignModule),
    },
    // {
    //     path: 'services/social-media-marketing',
    //     loadChildren: (): any =>
    //         import('./social-media-marketing/social-media-marketing.module').then(
    //             (m: any) => m.SocialMediaMarketingModule
    //         ),
    // },
    {
        path: 'about-us',
        loadChildren: (): any => import('./about-us/about-us.module').then((m: any) => m.AboutUsModule),
    },
    {
        path: 'contact',
        loadChildren: (): any => import('./contact/contact.module').then((m: any) => m.ContactModule),
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            initialNavigation: 'enabled',
            relativeLinkResolution: 'legacy',
            scrollPositionRestoration: 'enabled',
            anchorScrolling: 'enabled',
            scrollOffset: [0, 75],
        }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
