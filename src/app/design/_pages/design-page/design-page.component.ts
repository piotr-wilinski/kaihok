import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, takeUntil, tap } from 'rxjs';
import { DeviceDetectorService } from 'src/app/_core/_services/_device-detector/device-detector.service';
import { SearchEngineOptimizationService } from 'src/app/_core/_services/_search-engine-optimization/search-engine-optimization.service';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'pwd-design-page',
    templateUrl: './design-page.component.html',
    styleUrls: ['./design-page.component.scss'],
})
export class DesignPageComponent implements OnDestroy, OnInit {
    deviceType: DeviceType;
    private readonly _destroy$: Subject<void> = new Subject();

    constructor(
        private readonly _deviceDetectorService: DeviceDetectorService,
        private readonly _seoService: SearchEngineOptimizationService,
        private readonly _router: Router
    ) {
        this._deviceDetectorService.viewMode$
            .pipe(
                takeUntil(this._destroy$),
                tap((deviceType: DeviceType) => {
                    this.deviceType = deviceType;
                })
            )
            .subscribe();
    }

    ngOnInit(): void {
        this.setMetaTags();
    }

    setMetaTags(): void {
        this._seoService.setMetaTagsForPage(
            environment.appUrl + this._router.url,
            'Design Services | Kaihok',
            'Our unique approach,experience and passion combined, results in eye-catching,engaging and high converting designs'
        );
    }

    ngOnDestroy(): void {
        this._destroy$.next();
        this._destroy$.complete();
    }
}
