import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesignRoutingModule } from './design-routing.module';
import { DesignPageComponent } from './_pages/design-page/design-page.component';
import { BannerComponent } from './_views/banner/banner.component';
import { SharedModule } from '../_shared/shared.module';
import { ServicesViewComponent } from './_views/services-view/services-view.component';
import { AdvantagesViewComponent } from './_views/advantages-view/advantages-view.component';
import { WhyProvideServicesViewComponent } from './_views/why-provide-services-view/why-provide-services-view.component';

@NgModule({
    declarations: [
        DesignPageComponent,
        BannerComponent,
        ServicesViewComponent,
        AdvantagesViewComponent,
        WhyProvideServicesViewComponent,
    ],
    imports: [CommonModule, DesignRoutingModule, SharedModule],
})
export class DesignModule {}
