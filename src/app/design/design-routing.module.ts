import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DesignPageComponent } from './_pages/design-page/design-page.component';

const routes: Routes = [
    {
        path: '',
        component: DesignPageComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DesignRoutingModule {}
