import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'pwd-services-view',
    templateUrl: './services-view.component.html',
    styleUrls: ['./services-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServicesViewComponent {}
