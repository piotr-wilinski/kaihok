import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Component({
    selector: 'pwd-why-provide-services-view',
    templateUrl: './why-provide-services-view.component.html',
    styleUrls: ['./why-provide-services-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WhyProvideServicesViewComponent {
    @Input() deviceType: DeviceType;
    devicesList: typeof DeviceType = DeviceType;
}
