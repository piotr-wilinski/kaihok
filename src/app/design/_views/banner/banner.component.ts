import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Component({
    selector: 'pwd-banner',
    templateUrl: './banner.component.html',
    styleUrls: ['./banner.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BannerComponent {
    @Input() deviceType: DeviceType;
    currentRoute: string = 'Design';
    devicesList: typeof DeviceType = DeviceType;

    constructor(private readonly _router: Router) {}

    routeChangeListener(route: string): void {
        this._router.navigateByUrl(route);
    }
}
