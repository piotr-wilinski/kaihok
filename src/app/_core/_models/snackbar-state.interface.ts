export interface SnackbarState {
    message: string;
    secondMessage: string;
    showSnackbar: boolean;
    timeout: number;
    type?: 'success' | 'danger';
}
