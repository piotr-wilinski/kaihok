import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class NewsletterService {
    constructor(private readonly _httpClient: HttpClient) {}

    sendMail(email: string): Observable<void> {
        return this._httpClient.post<void>(`${environment.apiUrl}/audience/add/member`, {
            email,
        });
    }
}
