import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { DeviceType } from '../../_utils/_enums/device-type.enum';
import { DESKTOP_UP_BREAKPOINT_WIDTH, TABLET_UP_BREAKPOINT_WIDTH } from '../../_utils/_variables/device-breakpoints';

@Injectable({
    providedIn: 'root',
})
export class DeviceDetectorService {
    readonly DESKTOP_UP_BREAKPOINT_WIDTH: number = DESKTOP_UP_BREAKPOINT_WIDTH;
    readonly TABLET_UP_BREAKPOINT_WIDTH: number = TABLET_UP_BREAKPOINT_WIDTH;

    private readonly _deviceType$: BehaviorSubject<DeviceType> = new BehaviorSubject(DeviceType.MOBILE);

    public readonly isTabletUp$: Observable<boolean> = this._deviceType$
        .asObservable()
        .pipe(map((type: DeviceType) => type === DeviceType.TABLET_UP || type === DeviceType.DESKTOP_UP));
    public readonly isDesktopUp$: Observable<boolean> = this._deviceType$
        .asObservable()
        .pipe(map((type: DeviceType) => type === DeviceType.DESKTOP_UP));

    public readonly viewMode$: Observable<DeviceType> = this._deviceType$.asObservable().pipe();

    private setNewDeviceValue(deviceType: DeviceType): void {
        this._deviceType$.value !== deviceType && this._deviceType$.next(deviceType);
    }

    public checkResolution(): void {
        const isTabletUp: boolean = window.matchMedia(
            `screen and (min-width: ${this.TABLET_UP_BREAKPOINT_WIDTH}px)`
        ).matches;
        if (isTabletUp) {
            const isDesktopUp: boolean = window.matchMedia(
                `screen and (min-width: ${this.DESKTOP_UP_BREAKPOINT_WIDTH}px)`
            ).matches;
            this.setNewDeviceValue(!isDesktopUp ? DeviceType.TABLET_UP : DeviceType.DESKTOP_UP);
        } else {
            this.setNewDeviceValue(DeviceType.MOBILE);
        }
    }
}
