import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { MetaTag } from '../../_models/meta-tag.interface';

@Injectable({
    providedIn: 'root',
})
export class SearchEngineOptimizationService {
    private urlMeta: string = 'og:url';
    private titleMeta: string = 'og:title';
    private descriptionMeta: string = 'og:description';
    private imageMeta: string = 'og:image';
    private secureImageMeta: string = 'og:image:secure_url';
    private twitterTitleMeta: string = 'twitter:title';
    private twitterTitleTextMeta: string = 'twitter:text:title';
    private twitterDescMeta: string = 'twitter:description';
    private twitterImageMeta: string = 'twitter:image';

    constructor(
        private readonly _tittleService: Title,
        private readonly _metaService: Meta,
        @Inject(DOCUMENT) private readonly _document: any
    ) {}

    setTitle(title: string): void {
        this._tittleService.setTitle(title);
    }

    updateDescription(description: string): void {
        this._metaService.updateTag({ name: 'description', content: description });
    }

    updateCanonicalUrl(url: string): void {
        let element: HTMLLinkElement = this._document.querySelector(`link[rel='canonical']`) || null;
        if (!element) {
            const head: HTMLHeadElement = this._document.getElementsByTagName('head')[0];
            element = this._document.createLinkt('link') as HTMLLinkElement;
            head.appendChild(element);
        }
        element.setAttribute('rel', 'canonical');
        element.setAttribute('href', url);
    }

    setSocialMediaTags(url: string, title: string, description: string, image: string): void {
        const metaTags: Array<MetaTag> = [
            new MetaTag(this.urlMeta, url, true),
            new MetaTag(this.titleMeta, title, true),
            new MetaTag(this.descriptionMeta, description, true),
            new MetaTag(this.imageMeta, image, true),
            new MetaTag(this.secureImageMeta, image, true),
            new MetaTag(this.twitterTitleMeta, title, false),
            new MetaTag(this.twitterTitleTextMeta, title, false),
            new MetaTag(this.twitterDescMeta, description, false),
            new MetaTag(this.twitterImageMeta, image, false),
        ];
        this.setMetaTags(metaTags);
    }

    private setMetaTags(metaTags: Array<MetaTag>): void {
        metaTags.forEach((metaTag: MetaTag) => {
            if (metaTag.isFacebook) {
                this._metaService.updateTag({
                    property: metaTag.name,
                    content: metaTag.value,
                });
            } else {
                this._metaService.updateTag({
                    name: metaTag.name,
                    content: metaTag.value,
                });
            }
        });
    }

    setMetaTagsForPage(canonicalUrl: string, title: string, description: string, imageSrc?: string): void {
        const image: string = imageSrc;
        this.updateCanonicalUrl(canonicalUrl);
        this.updateDescription(description);
        this.setTitle(title);
        this.setSocialMediaTags(canonicalUrl, title, description, image);
    }
}
