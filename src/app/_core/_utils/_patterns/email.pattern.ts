export const EMAIL_PATTERN: RegExp = new RegExp('^[A-z0-9._%+-]+@[A-z0-9.-]+\\.[A-z]{2,}$');
