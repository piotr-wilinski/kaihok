export const TABLET_UP_BREAKPOINT_WIDTH: number = 768;
export const DESKTOP_UP_BREAKPOINT_WIDTH: number = 1024;
