import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OnlineMarketingPageComponent } from './_pages/online-marketing-page/online-marketing-page.component';

const routes: Routes = [
    {
        path: '',
        component: OnlineMarketingPageComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class OnlineMarketingRoutingModule {}
