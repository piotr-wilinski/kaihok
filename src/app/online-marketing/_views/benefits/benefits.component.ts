import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Component({
    selector: 'pwd-benefits',
    templateUrl: './benefits.component.html',
    styleUrls: ['./benefits.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BenefitsComponent {
    @Input() deviceType: DeviceType;
    devicesList: typeof DeviceType = DeviceType;
}
