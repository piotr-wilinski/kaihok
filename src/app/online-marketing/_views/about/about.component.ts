import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Component({
    selector: 'pwd-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AboutComponent {
    @Input() deviceType: DeviceType;
    devicesList: typeof DeviceType = DeviceType;
}
