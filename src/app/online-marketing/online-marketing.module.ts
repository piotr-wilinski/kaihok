import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnlineMarketingPageComponent } from './_pages/online-marketing-page/online-marketing-page.component';
import { OnlineMarketingRoutingModule } from './online-marketing-routing.module';
import { BannerComponent } from './_views/banner/banner.component';
import { SharedModule } from '../_shared/shared.module';
import { ServicesComponent } from './_views/services/services.component';
import { AboutComponent } from './_views/about/about.component';
import { BenefitsComponent } from './_views/benefits/benefits.component';

@NgModule({
    declarations: [OnlineMarketingPageComponent, BannerComponent, ServicesComponent, AboutComponent, BenefitsComponent],
    imports: [CommonModule, OnlineMarketingRoutingModule, SharedModule],
})
export class OnlineMarketingModule {}
