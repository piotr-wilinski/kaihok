import { isPlatformBrowser } from '@angular/common';
import { Component, Inject, PLATFORM_ID } from '@angular/core';
import { fromEvent, tap } from 'rxjs';
import { DeviceDetectorService } from './_core/_services/_device-detector/device-detector.service';

@Component({
    selector: 'pwd-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    isTabletUp: boolean;
    isDesktopUp: boolean;

    constructor(
        private readonly _deviceDetectorService: DeviceDetectorService,
        @Inject(PLATFORM_ID) private readonly _platformId: object
    ) {}

    ngOnInit(): void {
        if (isPlatformBrowser(this._platformId)) {
            this._deviceDetectorService.checkResolution();
            fromEvent(window, 'resize')
                .pipe(
                    tap(() => {
                        this._deviceDetectorService.checkResolution();
                    })
                )
                .subscribe();
            this._deviceDetectorService.isTabletUp$.subscribe((isTabletUp: boolean) => (this.isTabletUp = isTabletUp));
            this._deviceDetectorService.isDesktopUp$.subscribe(
                (isDesktopUp: boolean) => (this.isDesktopUp = isDesktopUp)
            );
        }
    }
}
