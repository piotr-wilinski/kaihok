import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { catchError, finalize, Subject, Subscription, takeUntil, tap, throwError } from 'rxjs';
import { DeviceDetectorService } from 'src/app/_core/_services/_device-detector/device-detector.service';
import { MailService } from 'src/app/_core/_services/_mail/mail.service';
import { SearchEngineOptimizationService } from 'src/app/_core/_services/_search-engine-optimization/search-engine-optimization.service';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';
import { EMAIL_PATTERN } from 'src/app/_core/_utils/_patterns/email.pattern';
import { SharedService } from 'src/app/_shared/shared.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'pwd-contact-page',
    templateUrl: './contact-page.component.html',
    styleUrls: ['./contact-page.component.scss'],
})
export class ContactPageComponent implements OnDestroy, OnInit {
    contactForm: FormGroup;
    deviceType: DeviceType;
    formSending: boolean = false;
    private readonly _destroy$: Subject<void> = new Subject();
    private readonly _subscription: Subscription = new Subscription();

    constructor(
        private readonly _deviceDetectorService: DeviceDetectorService,
        private readonly _mailService: MailService,
        private readonly _sharedService: SharedService,
        private readonly _seoService: SearchEngineOptimizationService,
        private readonly _router: Router
    ) {
        this._deviceDetectorService.viewMode$
            .pipe(
                takeUntil(this._destroy$),
                tap((deviceType: DeviceType) => {
                    this.deviceType = deviceType;
                })
            )
            .subscribe();
    }

    ngOnInit(): void {
        this.initContactForm();
        this.setMetaTags();
    }

    setMetaTags(): void {
        this._seoService.setMetaTagsForPage(
            environment.appUrl + this._router.url,
            'Contact Us | Kaihok',
            'Are you looking for new clients that will make your company grow? Contact us and let us do the work.'
        );
    }

    initContactForm(): void {
        this.contactForm = new FormGroup({
            name: new FormControl('', Validators.required),
            email: new FormControl('', [Validators.required, Validators.pattern(EMAIL_PATTERN)]),
            subject: new FormControl('', Validators.required),
            companyName: new FormControl(''),
            message: new FormControl('', Validators.required),
        });
    }

    onSubmitListener(): void {
        this.contactForm.valid ? this.sendMail() : this.contactForm.markAllAsTouched();
    }

    sendMail(): void {
        this.formSending = true;
        this._subscription.add(
            this._mailService
                .sendMail(this.contactForm.value)
                .pipe(
                    tap(() => {
                        this._sharedService.showSnackbar(
                            'Message sent successfully',
                            'We will contact you soon',
                            'success'
                        );
                        this.contactForm.reset();
                    }),
                    catchError((error: HttpErrorResponse) => {
                        this._sharedService.showSnackbar(
                            'There was an error sending the message',
                            'Please try again later',
                            'danger'
                        );
                        return throwError(error);
                    }),
                    finalize(() => (this.formSending = false))
                )
                .subscribe()
        );
    }

    ngOnDestroy(): void {
        this._destroy$.next();
        this._destroy$.complete();
    }
}
