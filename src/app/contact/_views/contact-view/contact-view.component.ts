import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Component({
    selector: 'pwd-contact-view',
    templateUrl: './contact-view.component.html',
    styleUrls: ['./contact-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactViewComponent {
    @Input() contactForm: FormGroup;
    @Input() deviceType: DeviceType;
    @Input() formSending: boolean = false;
    @Output() onSubmitEmitter: EventEmitter<void> = new EventEmitter<void>();

    getErrors(formControl: string, isMail?: boolean): boolean {
        if (isMail && this.contactForm.get(formControl).errors?.pattern)
            return this.contactForm.get(formControl).touched && this.contactForm.get(formControl).errors?.pattern;
        return this.contactForm.get(formControl).touched && this.contactForm.get(formControl).errors?.required;
    }

    onSubmit(): void {
        this.contactForm.valid ? this.onSubmitEmitter.emit() : this.contactForm.markAllAsTouched();
    }
}
