import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactPageComponent } from './_pages/contact-page/contact-page.component';
import { ContactViewComponent } from './_views/contact-view/contact-view.component';
import { ContactRoutingModule } from './contact-routing.module';
import { SharedModule } from '../_shared/shared.module';

@NgModule({
    declarations: [ContactPageComponent, ContactViewComponent],
    imports: [CommonModule, ContactRoutingModule, SharedModule],
})
export class ContactModule {}
