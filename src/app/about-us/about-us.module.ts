import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutUsPageComponent } from './_pages/about-us-page/about-us-page.component';
import { AboutUsRoutingModule } from './about-us-routing.module';
import { SharedModule } from '../_shared/shared.module';
import { BannerViewComponent } from './_views/banner-view/banner-view.component';
import { AboutKaihokViewComponent } from './_views/about-kaihok-view/about-kaihok-view.component';
import { HowWeWorkViewComponent } from './_views/how-we-work-view/how-we-work-view.component';

@NgModule({
    declarations: [AboutUsPageComponent, BannerViewComponent, AboutKaihokViewComponent, HowWeWorkViewComponent],
    imports: [CommonModule, AboutUsRoutingModule, SharedModule],
})
export class AboutUsModule {}
