import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'pwd-how-we-work-view',
    templateUrl: './how-we-work-view.component.html',
    styleUrls: ['./how-we-work-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HowWeWorkViewComponent {}
