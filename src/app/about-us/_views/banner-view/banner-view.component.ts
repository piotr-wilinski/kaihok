import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'pwd-banner-view',
    templateUrl: './banner-view.component.html',
    styleUrls: ['./banner-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BannerViewComponent {}
