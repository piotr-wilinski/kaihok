import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Component({
    selector: 'pwd-about-kaihok-view',
    templateUrl: './about-kaihok-view.component.html',
    styleUrls: ['./about-kaihok-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AboutKaihokViewComponent {
    @Input() deviceType: DeviceType;
    devicesList: typeof DeviceType = DeviceType;
}
