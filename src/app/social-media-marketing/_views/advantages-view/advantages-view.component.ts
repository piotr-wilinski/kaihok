import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'pwd-advantages-view',
    templateUrl: './advantages-view.component.html',
    styleUrls: ['./advantages-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdvantagesViewComponent {}
