import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Component({
    selector: 'pwd-banner-view',
    templateUrl: './banner-view.component.html',
    styleUrls: ['./banner-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BannerViewComponent {
    @Input() deviceType: DeviceType;
    currentRoute: string = 'Social Media Marketing';
    devicesList: typeof DeviceType = DeviceType;

    constructor(private readonly _router: Router) {}

    routeChangeListener(route: string): void {
        this._router.navigateByUrl(route);
    }
}
