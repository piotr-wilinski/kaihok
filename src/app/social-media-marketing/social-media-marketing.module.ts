import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialMediaMarketingPageComponent } from './_pages/social-media-marketing-page/social-media-marketing-page.component';
import { SocialMediaMarketingRoutingModule } from './social-media-marketing-routing.module';
import { BannerViewComponent } from './_views/banner-view/banner-view.component';
import { SharedModule } from '../_shared/shared.module';
import { ServicesViewComponent } from './_views/services-view/services-view.component';
import { AdvantagesViewComponent } from './_views/advantages-view/advantages-view.component';

@NgModule({
    declarations: [
        SocialMediaMarketingPageComponent,
        BannerViewComponent,
        ServicesViewComponent,
        AdvantagesViewComponent,
    ],
    imports: [CommonModule, SocialMediaMarketingRoutingModule, SharedModule],
})
export class SocialMediaMarketingModule {}
