import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SocialMediaMarketingPageComponent } from './_pages/social-media-marketing-page/social-media-marketing-page.component';

const routes: Routes = [
    {
        path: '',
        component: SocialMediaMarketingPageComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SocialMediaMarketingRoutingModule {}
