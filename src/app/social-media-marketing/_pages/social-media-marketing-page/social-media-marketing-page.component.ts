import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, takeUntil, tap } from 'rxjs';
import { DeviceDetectorService } from 'src/app/_core/_services/_device-detector/device-detector.service';
import { SearchEngineOptimizationService } from 'src/app/_core/_services/_search-engine-optimization/search-engine-optimization.service';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'pwd-social-media-marketing-page',
    templateUrl: './social-media-marketing-page.component.html',
    styleUrls: ['./social-media-marketing-page.component.scss'],
})
export class SocialMediaMarketingPageComponent implements OnDestroy, OnInit {
    deviceType: DeviceType;
    private readonly _destroy$: Subject<void> = new Subject();

    constructor(
        private readonly _deviceDetectorService: DeviceDetectorService,
        private readonly _seoService: SearchEngineOptimizationService,
        private readonly _router: Router
    ) {
        this._deviceDetectorService.viewMode$
            .pipe(
                takeUntil(this._destroy$),
                tap((deviceType: DeviceType) => {
                    this.deviceType = deviceType;
                })
            )
            .subscribe();
    }

    ngOnInit(): void {
        this.setMetaTags();
    }

    setMetaTags(): void {
        this._seoService.setMetaTagsForPage(
            environment.appUrl + this._router.url,
            'Social Media Marketing Services | Kaihok',
            'Increase brand awareness and inbound traffic using our Social Media Marketing services. Grow your brand through many social platforms such as; Facebook, Instagram, LinkedIn'
        );
    }

    ngOnDestroy(): void {
        this._destroy$.next();
        this._destroy$.complete();
    }
}
