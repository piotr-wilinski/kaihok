import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Component({
    selector: 'pwd-professions-view',
    templateUrl: './professions-view.component.html',
    styleUrls: ['./professions-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfessionsViewComponent {
    @Input() deviceType: DeviceType;
    devicesList: typeof DeviceType = DeviceType;
}
