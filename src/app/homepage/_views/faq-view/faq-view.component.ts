import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Component({
    selector: 'pwd-faq-view',
    templateUrl: './faq-view.component.html',
    styleUrls: ['./faq-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaqViewComponent {
    @Input() deviceType: DeviceType;
    devicesList: typeof DeviceType = DeviceType;
    showAnswerId: number = 0;

    showAnswer(answerId: number): void {
        answerId !== this.showAnswerId ? (this.showAnswerId = answerId) : (this.showAnswerId = 0);
    }
}
