import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Component({
    selector: 'pwd-hero-view',
    templateUrl: './hero-view.component.html',
    styleUrls: ['./hero-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeroViewComponent {
    @Input() deviceType: DeviceType;
    devicesList: typeof DeviceType = DeviceType;
}
