import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'pwd-why-us-view',
    templateUrl: './why-us-view.component.html',
    styleUrls: ['./why-us-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WhyUsViewComponent {}
