import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Component({
    selector: 'pwd-who-are-we-view',
    templateUrl: './who-are-we-view.component.html',
    styleUrls: ['./who-are-we-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WhoAreWeViewComponent {
    @Input() deviceType: DeviceType;
    devicesList: typeof DeviceType = DeviceType;
}
