import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil, tap } from 'rxjs';
import { DeviceDetectorService } from 'src/app/_core/_services/_device-detector/device-detector.service';
import { SearchEngineOptimizationService } from 'src/app/_core/_services/_search-engine-optimization/search-engine-optimization.service';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Component({
    selector: 'pwd-homepage',
    templateUrl: './homepage.component.html',
    styleUrls: ['./homepage.component.scss'],
})
export class HomepageComponent implements OnDestroy, OnInit {
    deviceType: DeviceType;
    private readonly _destroy$: Subject<void> = new Subject();

    constructor(
        private readonly _deviceDetectorService: DeviceDetectorService,
        private readonly _seoService: SearchEngineOptimizationService
    ) {
        this._deviceDetectorService.viewMode$
            .pipe(
                takeUntil(this._destroy$),
                tap((deviceType: DeviceType) => {
                    this.deviceType = deviceType;
                })
            )
            .subscribe();
    }

    ngOnInit(): void {
        this.setMetaTags();
    }

    setMetaTags(): void {
        this._seoService.setMetaTagsForPage(
            'https://kaihok.com',
            'Kaihok | Online Marketing & Web Design for Home Service and Construction companies',
            'Kaihok is an digital agency providing online marketing & web design services to home service and construction companies. We help increase revenue by bringing high converting leads'
        );
    }

    ngOnDestroy(): void {
        this._destroy$.next();
        this._destroy$.complete();
    }
}
