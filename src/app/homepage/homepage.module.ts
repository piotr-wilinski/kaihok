import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageRoutingModule } from './homepage-routing.module';
import { HomepageComponent } from './_pages/homepage/homepage.component';
import { FaqViewComponent } from './_views/faq-view/faq-view.component';
import { HeroViewComponent } from './_views/hero-view/hero-view.component';
import { ProfessionsViewComponent } from './_views/professions-view/professions-view.component';
import { ServicesViewComponent } from './_views/services-view/services-view.component';
import { WhoAreWeViewComponent } from './_views/who-are-we-view/who-are-we-view.component';
import { WhyUsViewComponent } from './_views/why-us-view/why-us-view.component';
import { SharedModule } from '../_shared/shared.module';

@NgModule({
    declarations: [
        HomepageComponent,
        FaqViewComponent,
        HeroViewComponent,
        ProfessionsViewComponent,
        ServicesViewComponent,
        WhoAreWeViewComponent,
        WhyUsViewComponent,
    ],
    imports: [CommonModule, HomepageRoutingModule, SharedModule],
})
export class HomepageModule {}
