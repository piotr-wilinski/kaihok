import { isPlatformBrowser } from '@angular/common';
import { Directive, ElementRef, Inject, PLATFORM_ID, Renderer2 } from '@angular/core';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

const FRACTION_OF_VIEWPORT_HEIGHT: number = 0.25;

@Directive({
    selector: '[pwdFadeWhenAvailable]',
})
export class FadeWhenAvailableDirective {
    listenerUnsub: any;
    deviceType: DeviceType;

    constructor(
        private readonly _renderer: Renderer2,
        private readonly _elementRef: ElementRef,
        @Inject(PLATFORM_ID) private readonly _platformId: object
    ) {}

    ngOnInit(): void {
        isPlatformBrowser(this._platformId) && this.initStyles();
    }

    initStyles(): void {
        this._renderer.setStyle(this._elementRef.nativeElement, 'opacity', 0);
        this._renderer.setStyle(this._elementRef.nativeElement, 'transition', 'opacity 1s');

        this.listenerUnsub = this._renderer.listen(document, 'scroll', () => this.checkIsElementReadyToShow());
    }

    checkIsElementReadyToShow(): void {
        const offsetElementFromTopDocument: number = this._elementRef.nativeElement.getBoundingClientRect().top;
        const breakpointViewportValueToShowElement: number =
            window.innerHeight - window.innerHeight * FRACTION_OF_VIEWPORT_HEIGHT;
        if (offsetElementFromTopDocument < breakpointViewportValueToShowElement) this.showElement();
    }

    showElement(): void {
        this._renderer.setStyle(this._elementRef.nativeElement, 'opacity', 1);
        this.listenerUnsub();
    }

    ngOnDestroy(): void {
        if (this.listenerUnsub) this.listenerUnsub();
    }
}
