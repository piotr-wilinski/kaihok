import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReusableContactFormComponent } from './_components/reusable-contact-form/reusable-contact-form.component';
import { ResponsiveImagePipe } from './_pipes/responsive-image.pipe';
import { WebpSupportPipe } from './_pipes/webp-support.pipe';
import { FadeWhenAvailableDirective } from './_directives/fade-when-available.directive';
import { SnackbarComponent } from './_components/snackbar/snackbar.component';
import { ProgressSpinnerComponent } from './_components/progress-spinner/progress-spinner.component';
import { ServicesNavigationMenuComponent } from './_components/services-navigation-menu/services-navigation-menu.component';

@NgModule({
    declarations: [
        WebpSupportPipe,
        ResponsiveImagePipe,
        ReusableContactFormComponent,
        FadeWhenAvailableDirective,
        SnackbarComponent,
        ProgressSpinnerComponent,
        ServicesNavigationMenuComponent,
    ],
    imports: [CommonModule, FormsModule, ReactiveFormsModule],
    exports: [
        WebpSupportPipe,
        ResponsiveImagePipe,
        ReusableContactFormComponent,
        FormsModule,
        ReactiveFormsModule,
        FadeWhenAvailableDirective,
        SnackbarComponent,
        ProgressSpinnerComponent,
        ServicesNavigationMenuComponent,
    ],
})
export class SharedModule {}
