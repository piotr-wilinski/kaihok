import { ChangeDetectionStrategy, Component, Input, OnDestroy } from '@angular/core';
import { IsActiveMatchOptions } from '@angular/router';
import { Subject, takeUntil, tap } from 'rxjs';
import { DeviceDetectorService } from 'src/app/_core/_services/_device-detector/device-detector.service';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Component({
    selector: 'pwd-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnDestroy {
    @Input() isDesktopUp: boolean;
    private readonly _destroy$: Subject<void> = new Subject();
    deviceType: DeviceType;
    isMenuOpen: boolean = false;
    readonly activeRouteMatchOptions: IsActiveMatchOptions = {
        queryParams: 'ignored',
        matrixParams: 'exact',
        paths: 'exact',
        fragment: 'exact',
    };

    constructor(private readonly _deviceDetectorService: DeviceDetectorService) {
        this._deviceDetectorService.viewMode$
            .pipe(
                takeUntil(this._destroy$),
                tap((deviceType: DeviceType) => {
                    this.deviceType = deviceType;
                })
            )
            .subscribe();
    }
    closeMenu(): void {
        this.isMenuOpen = false;
    }

    onMenuClick(): void {
        this.isMenuOpen = !this.isMenuOpen;
    }

    ngOnDestroy(): void {
        this._destroy$.next();
        this._destroy$.complete();
    }
}
