import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { catchError, finalize, Subscription, tap, throwError } from 'rxjs';
import { MailService } from 'src/app/_core/_services/_mail/mail.service';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';
import { EMAIL_PATTERN } from 'src/app/_core/_utils/_patterns/email.pattern';
import { SharedService } from '../../shared.service';

@Component({
    selector: 'pwd-reusable-contact-form',
    templateUrl: './reusable-contact-form.component.html',
    styleUrls: ['./reusable-contact-form.component.scss'],
})
export class ReusableContactFormComponent implements OnDestroy, OnInit {
    @Input() deviceType: DeviceType;
    contactForm: FormGroup;
    devicesList: typeof DeviceType = DeviceType;
    formSending: boolean = false;
    private readonly _subscription: Subscription = new Subscription();

    constructor(private readonly _mailService: MailService, private readonly _sharedService: SharedService) {}

    ngOnInit(): void {
        this.initContactForm();
    }

    initContactForm(): void {
        this.contactForm = new FormGroup({
            name: new FormControl('', Validators.required),
            email: new FormControl('', [Validators.required, Validators.pattern(EMAIL_PATTERN)]),
            subject: new FormControl('', Validators.required),
            companyName: new FormControl(''),
            message: new FormControl('', Validators.required),
        });
    }

    getErrors(formControl: string, isMail?: boolean): boolean {
        if (isMail && this.contactForm.get(formControl).errors?.pattern)
            return this.contactForm.get(formControl).touched && this.contactForm.get(formControl).errors?.pattern;
        return this.contactForm.get(formControl).touched && this.contactForm.get(formControl).errors?.required;
    }

    onSubmit(): void {
        this.contactForm.valid ? this.sendMail() : this.contactForm.markAllAsTouched();
    }

    sendMail(): void {
        this.formSending = true;
        this._subscription.add(
            this._mailService
                .sendMail(this.contactForm.value)
                .pipe(
                    tap(() => {
                        this._sharedService.showSnackbar(
                            'Message sent successfully',
                            'We will contact you soon',
                            'success'
                        );
                        this.contactForm.reset();
                    }),
                    catchError((error: HttpErrorResponse) => {
                        this._sharedService.showSnackbar(
                            'There was an error sending the message',
                            'Please try again later',
                            'danger'
                        );
                        return throwError(error);
                    }),
                    finalize(() => (this.formSending = false))
                )
                .subscribe()
        );
    }

    ngOnDestroy(): void {
        this._subscription.unsubscribe();
    }
}
