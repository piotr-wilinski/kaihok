import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'pwd-progress-spinner',
    templateUrl: './progress-spinner.component.html',
    styleUrls: ['./progress-spinner.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgressSpinnerComponent {}
