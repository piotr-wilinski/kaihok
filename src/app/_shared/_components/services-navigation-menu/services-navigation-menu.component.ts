import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'pwd-services-navigation-menu',
    templateUrl: './services-navigation-menu.component.html',
    styleUrls: ['./services-navigation-menu.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServicesNavigationMenuComponent {
    @Input() currentRoute: string;
    @Output() routeChangeEmitter: EventEmitter<string> = new EventEmitter<string>();
    navOpen: boolean = false;

    onNavOpen(): void {
        this.navOpen = !this.navOpen;
    }
}
