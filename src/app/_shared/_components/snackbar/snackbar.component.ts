import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, switchMap, tap, timer } from 'rxjs';
import { SnackbarState } from 'src/app/_core/_models/snackbar-state.interface';
import { SharedService } from '../../shared.service';

@Component({
    selector: 'pwd-snackbar',
    templateUrl: './snackbar.component.html',
    styleUrls: ['./snackbar.component.scss'],
})
export class SnackbarComponent implements OnDestroy, OnInit {
    message: string;
    secondMessage: string;
    showSnackbar: boolean = false;
    type: string = 'success';
    private readonly _subscription: Subscription = new Subscription();

    constructor(private readonly _sharedService: SharedService) {}

    ngOnInit(): void {
        this.checkSnackbarState();
    }

    checkSnackbarState(): void {
        this._subscription.add(
            this._sharedService.snackbarState
                .pipe(
                    tap((state: SnackbarState) => {
                        this.type = state.type || 'success';
                        this.message = state.message;
                        this.secondMessage = state.secondMessage;
                        this.showSnackbar = state.showSnackbar;
                    }),
                    switchMap((state: SnackbarState) => {
                        return timer(state.timeout);
                    }),
                    tap(() => {
                        this.showSnackbar = false;
                    })
                )
                .subscribe()
        );
    }

    ngOnDestroy(): void {
        this._subscription.unsubscribe();
    }
}
