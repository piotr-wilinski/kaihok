import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { catchError, finalize, Subscription, tap, throwError } from 'rxjs';
import { NewsletterService } from 'src/app/_core/_services/_newsletter/newsletter.service';
import { EMAIL_PATTERN } from 'src/app/_core/_utils/_patterns/email.pattern';
import { SharedService } from '../../shared.service';

@Component({
    selector: 'pwd-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnDestroy, OnInit {
    readonly CURRENT_DATE: Date = new Date();
    email: FormControl;
    newsletterSigning: boolean = false;
    private readonly _subscription: Subscription = new Subscription();

    constructor(
        private readonly _newsletterService: NewsletterService,
        private readonly _sharedService: SharedService
    ) {}

    ngOnInit(): void {
        this.email = new FormControl(null, [Validators.required, Validators.pattern(EMAIL_PATTERN)]);
    }

    readyToSignUpToNewsletter(): void {
        this.email.valid ? this.signUpToNewsletter() : this.email.markAsTouched();
    }

    signUpToNewsletter(): void {
        this.newsletterSigning = true;
        this._subscription.add(
            this._newsletterService
                .sendMail(this.email.value)
                .pipe(
                    tap(() => {
                        this._sharedService.showSnackbar(
                            'You have successfully subscribed to the newsletter',
                            null,
                            'success'
                        );
                    }),
                    catchError((error: HttpErrorResponse) => {
                        console.log(error);
                        if (error.error.response.text.includes('Member Exists')) {
                            this._sharedService.showSnackbar('Member Exists', null, 'danger');
                        } else {
                            this._sharedService.showSnackbar(
                                'There was an error while subscribing to the newsletter',
                                'Please try again later',
                                'danger'
                            );
                        }
                        return throwError(error);
                    }),
                    finalize(() => ((this.newsletterSigning = false), this.email.reset()))
                )
                .subscribe()
        );
    }

    ngOnDestroy(): void {
        this._subscription.unsubscribe();
    }
}
