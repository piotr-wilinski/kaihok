import { Pipe, PipeTransform } from '@angular/core';
import { DeviceType } from 'src/app/_core/_utils/_enums/device-type.enum';

@Pipe({
    name: 'responsiveImage',
})
export class ResponsiveImagePipe implements PipeTransform {
    transform(value: string, type: string, deviceList?: DeviceType[]): string {
        let endPath: string = value;
        if (type === DeviceType.MOBILE && deviceList?.includes(DeviceType.MOBILE)) {
            endPath += '-mobile';
        } else {
            endPath += '-desktop';
        }
        return endPath;
    }
}
