import { isPlatformServer } from '@angular/common';
import { Inject, Pipe, PipeTransform, PLATFORM_ID } from '@angular/core';
import { SharedService } from '../shared.service';

@Pipe({
    name: 'webpSupport',
})
export class WebpSupportPipe implements PipeTransform {
    constructor(
        @Inject(PLATFORM_ID) private readonly _platformId: object,
        private readonly _sharedService: SharedService
    ) {}

    transform(imageUrlWithoutFormat: string, alternativeFormat: string): string {
        let format: string;
        if (isPlatformServer(this._platformId) || this._sharedService.isCompatibleWithWebp()) {
            format = 'webp';
        } else {
            format = alternativeFormat;
        }
        return `${imageUrlWithoutFormat}.${format}`;
    }
}
