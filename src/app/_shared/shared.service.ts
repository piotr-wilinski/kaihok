import { isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class SharedService {
    private _browserCompatibleWithWebP: boolean = false;
    private _snackbarSubject: Subject<any> = new Subject<any>();

    public snackbarState: Observable<any> = this._snackbarSubject.asObservable();

    constructor(@Inject(PLATFORM_ID) private readonly _platformId: object) {
        this.checkBrowserCompatibilityWithWebP();
    }

    checkBrowserCompatibilityWithWebP(): void {
        if (isPlatformBrowser(this._platformId)) {
            const elem: HTMLCanvasElement = document.createElement('canvas');
            if (elem.getContext?.('2d')) {
                const testString: string = !((window as any).mozInnerScreenX == null) ? 'png' : 'webp';
                this._browserCompatibleWithWebP = elem.toDataURL('image/webp').indexOf('data:image/' + testString) == 0;
            }
        } else {
            this._browserCompatibleWithWebP = true;
        }
    }

    isCompatibleWithWebp(): boolean {
        return this._browserCompatibleWithWebP;
    }

    showSnackbar(message: string, secondMessage: string, type?: string, timeout: number = 3000): void {
        this._snackbarSubject.next({
            showSnackbar: true,
            message,
            secondMessage,
            type,
            timeout,
        });
    }
}
