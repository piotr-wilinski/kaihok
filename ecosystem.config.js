module.exports = {
    apps: [
        {
            name: 'kaihok',
            script: 'dist/kaihok/server/main.js',
            error_file: 'errors/err.log',
            out_file: 'errors/out.log',
            log_file: 'errors/combined.log',
            time: true,
            env: {
                PORT: 4030,
            },
        },
    ],
};
